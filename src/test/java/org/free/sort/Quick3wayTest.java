package org.free.sort;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.free.sort.TestUtil.getRandomArray;
import static org.free.sort.TestUtil.getTime;
import static org.junit.jupiter.api.Assertions.*;

class Quick3wayTest {
    @Test
    void sort() {
        int num = 10;
        Double[] randomArray = getRandomArray(num);
        Sortable quick = new Quick3way(Arrays.copyOf(randomArray, randomArray.length, Double[].class));
        quick.sort();
        quick.show();
        assertTrue(quick.isSorted());
        System.out.println(">>>  " + getTime(Arrays.copyOf(randomArray, randomArray.length, Double[].class),
                SortType.Merge));
    }

}