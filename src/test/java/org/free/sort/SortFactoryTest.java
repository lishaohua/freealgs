package org.free.sort;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SortFactoryTest {
    @Test
    void getInstance() {
        assertTrue(SortFactory.getInstance(SortType.Selection, new Comparable[1]) instanceof Selection);
        assertTrue(SortFactory.getInstance(SortType.Insertion, new Comparable[1]) instanceof Insertion);
        Comparable[] lC = new Comparable[] { new Comparable() {
            public int compareTo(Object o) {
                return 0;
            }
        }};
        assertTrue(SortFactory.getInstance(SortType.Insertion2, lC) instanceof Insertion2);
        assertTrue(SortFactory.getInstance(SortType.Insertion2, new String[]{"111"}) instanceof Insertion2);
    }

    public static void main(String[] args) {

    }

}