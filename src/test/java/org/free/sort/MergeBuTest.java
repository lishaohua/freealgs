package org.free.sort;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.free.sort.TestUtil.getRandomArray;
import static org.free.sort.TestUtil.getTime;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * 将递归方式的归并排序转为循环方式的归并排序
 * 该方法也是自底向上解释问题的思路
 */
class MergeBuTest {
    @Test
    void sort() {
        int num = 10;
        Double[] randomArray = getRandomArray(num);
        Sortable merge = new MergeBu(Arrays.copyOf(randomArray, randomArray.length, Double[].class));
        merge.sort();
        merge.show();
        assertTrue(merge.isSorted());
        System.out.println(">>>  " + getTime(Arrays.copyOf(randomArray, randomArray.length, Double[].class),
                SortType.Merge));
    }

}