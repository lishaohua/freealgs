package org.free.sort;

import static org.free.sort.TestUtil.*;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class InsertionTest {


    @Test
    void sort() {
        Comparable<Integer>[] a = new Comparable[]{6, 8, 2, 3, 1, 7, 5, 4};
        Sortable sortable = new Insertion(a);
        sortable.sort();
        assertTrue(sortable.isSorted());
        sortable.show();
    }

    @Test
    void sort2() {
        Comparable<Integer>[] a = new Comparable[]{6, 8, 2, 3, 1, 7, 5, 4};
        Sortable sortable = new Insertion2(a);
        sortable.sort();
        assertTrue(sortable.isSorted());
        sortable.show();
    }

    @Test
    void compare() {
        int num = 100;
        Double[] randomArray = getRandomArray(num);
        System.out.println(">>>  " + getTime(Arrays.copyOf(randomArray, randomArray.length, Double[].class),
                SortType.Insertion));
        System.out.println(">>>  " + getTime(Arrays.copyOf(randomArray, randomArray.length, Double[].class),
                SortType.Insertion2));

    }




}