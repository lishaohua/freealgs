package org.free.sort;

import org.free.util.StdOut;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.free.sort.TestUtil.getRandomArray;
import static org.free.sort.TestUtil.getTime;
import static org.junit.jupiter.api.Assertions.assertTrue;

class MergeTest {


    @Test
    void sort() {
        int num = 10;
        Double[] randomArray = getRandomArray(num);
        Merge merge = new Merge(Arrays.copyOf(randomArray, randomArray.length, Double[].class));
        merge.sort();
        merge.show();
        assertTrue(merge.isSorted());
        System.out.println(">>>  " + getTime(Arrays.copyOf(randomArray, randomArray.length, Double[].class),
                SortType.Merge));
    }

    @Test
    public void timeTest() {
        Double[] randomArray = getRandomArray(10000000);
        double time = getTime(Arrays.copyOf(randomArray, randomArray.length, Double[].class),
                SortType.Shell);
        StdOut.printf("希尔排序时间为：%s \n", time);
        double time2 = getTime(Arrays.copyOf(randomArray, randomArray.length, Double[].class),
                SortType.Merge);
        StdOut.printf("归并排序时间为：%s \n", time2);
    }

    @Test
    public void merge() {
        Integer[] a = new Integer[]{4, 6, 8, 3, 5, 7};

        Merge merge = new Merge(a);
        merge.merge(0, 2, 5);
        merge.show();
        assertTrue(merge.isSorted());
    }

}