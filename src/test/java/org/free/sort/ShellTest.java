package org.free.sort;

import org.free.util.StdOut;
import org.free.util.Stopwatch;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.free.sort.TestUtil.*;

import static org.junit.jupiter.api.Assertions.*;

class ShellTest {


    @Test
    void sort() {
        Double[] randomArray = getRandomArray(1000000);
        Shell shell = new Shell(randomArray);
        Stopwatch timer = new Stopwatch();
        shell.sort();
        double elapsedTime = timer.elapsedTime();
        StdOut.printf("花费时间为 %s \n", elapsedTime);
        assertTrue(shell.isSorted());
    }

    @Test
    void compare() {
        Double[] randomArray = getRandomArray(100000);
        double time = getTime(Arrays.copyOf(randomArray, randomArray.length, Double[].class),
                SortType.Insertion);
        StdOut.printf("插入排序时间为：%s \n", time);
        double time2 = getTime(Arrays.copyOf(randomArray, randomArray.length, Double[].class),
                SortType.Shell);
        StdOut.printf("希尔排序时间为：%s \n", time2);

    }

}