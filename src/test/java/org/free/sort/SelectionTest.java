package org.free.sort;

import static org.junit.jupiter.api.Assertions.*;

class SelectionTest {
    @org.junit.jupiter.api.Test
    void sort() {
        Comparable<Integer>[] a = new Comparable[]{6, 8, 2, 3, 1};
        Sortable sortable = new Selection(a);
        sortable.sort();
        assertTrue(sortable.isSorted());
        sortable.show();
    }
}