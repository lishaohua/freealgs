package org.free.sort;

import org.free.util.StdRandom;
import org.free.util.Stopwatch;

public class TestUtil {
    public static double getTime(Double[] randomArray, SortType sortType) {
        Stopwatch timer = new Stopwatch();
        Sortable sortable = SortFactory.getInstance(sortType, randomArray);
        sortable.sort();
        return timer.elapsedTime();
    }

    public static Double[] getRandomArray(int num) {
        Double[] a = new Double[num];
        for (int i = 0; i < num; i++) {
            a[i] = StdRandom.uniform();
        }
        return a;
    }
}
