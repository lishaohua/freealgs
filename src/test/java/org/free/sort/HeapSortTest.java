package org.free.sort;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertTrue;

class HeapSortTest {
    @Test
    void sort() {
        String[] randomArray = " SORTEXAMPLE".split("");
        Sortable sort = new HeapSort(Arrays.copyOf(randomArray, randomArray.length, String[].class));
        sort.sort();
        sort.show();
        assertTrue(sort.isSorted());
    }
}