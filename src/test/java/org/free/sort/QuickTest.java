package org.free.sort;

import org.free.util.StdOut;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.free.sort.TestUtil.getRandomArray;
import static org.free.sort.TestUtil.getTime;
import static org.junit.jupiter.api.Assertions.*;

class QuickTest {
    @Test
    void sort() {
        int num = 10;
        Double[] randomArray = getRandomArray(num);
        Sortable quick = new Quick(Arrays.copyOf(randomArray, randomArray.length, Double[].class));
        quick.sort();
        quick.show();
        assertTrue(quick.isSorted());
        System.out.println(">>>  " + getTime(Arrays.copyOf(randomArray, randomArray.length, Double[].class),
                SortType.Merge));
    }

    @Test
    public void timeTest() {
        Double[] randomArray = getRandomArray(10000000);
        double time = getTime(Arrays.copyOf(randomArray, randomArray.length, Double[].class),
                SortType.Merge);
        StdOut.printf("归并排序时间为：%s \n", time);
        double time2 = getTime(Arrays.copyOf(randomArray, randomArray.length, Double[].class),
                SortType.Quick);
        StdOut.printf("快速排序时间为：%s \n", time2);
    }

}