package org.free.sort;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BubbleTest {


    @Test
    void sort() {
        Comparable<Integer>[] a = new Comparable[]{6, 8, 2, 3, 1, 7, 5, 4};
        Sortable sortable = new Bubble(a);
        sortable.show();
        sortable.sort();
        assertTrue(sortable.isSorted());
        sortable.show();
    }

}