package org.free.find;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class FindTest {
    @Test
    void bsearchFistGt() {
        Integer[] a = new Integer[]{3, 4, 5, 7, 8, 10, 11, 19};
        int bsearch = Find.bsearchFistGt(a, 6);
        assertEquals(3, bsearch);
    }

    @Test
    void bsearchFirst() {
        Integer[] a = new Integer[]{3, 4, 5, 5, 5, 6, 6, 6, 7, 8};
        int bsearch = Find.bsearchFirst(a, 6);
        assertEquals(5, bsearch);
    }

    @Test
    public void bsearchLast() {
        Integer[] a = new Integer[]{3, 4, 5, 5, 5, 6, 6, 6, 7, 8};
        int bsearch = Find.bsearchLast(a, 6);
        assertEquals(7, bsearch);
    }

    @Test
    void bsearch() {
        Integer[] a = new Integer[]{3, 4, 5, 6, 7, 8};
        int bsearch = Find.bsearch(a, 6);
        assertEquals(3, bsearch);
    }

}