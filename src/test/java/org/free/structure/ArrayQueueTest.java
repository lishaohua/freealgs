package org.free.structure;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ArrayQueueTest {
    @Test
    void enqueue() {
        ArrayQueue<String> queue = new ArrayQueue<String>(5);
        queue.enqueue("A");
        queue.enqueue("B");
        queue.enqueue("C");
        queue.enqueue("3");
        System.out.println(queue);
        String dequeue = queue.dequeue();
        System.out.println(queue);
        assertEquals("A", dequeue);
        assertEquals("B", queue.dequeue());
        queue.enqueue("$");
        queue.enqueue("5");
        queue.enqueue("6");
        queue.enqueue("7");
        queue.enqueue("9");
        System.out.println(queue);


    }

    @Test
    void dequeue() {
    }

}