package org.free.structure.link;

import org.junit.jupiter.api.Test;

class LinkedListTest {
    @Test
    void getMedian() {
        LinkedList<Integer> aList = new LinkedList<>();
        aList.addOrdered(9);
        aList.addOrdered(7);
        aList.addOrdered(5);
        aList.addOrdered(3);
        aList.addOrdered(1);
        System.out.println(aList);
        System.out.println(aList.getMedian().t);
        aList.addOrdered(4);
        aList.addOrdered(11);
        aList.addOrdered(12);
        aList.addOrdered(13);
        aList.addOrdered(14);
        System.out.println(aList);
        System.out.println(aList.getMedian().t);

    }


    @Test
    void remove() {
        LinkedList<Integer> aList = new LinkedList<>();
        aList.addOrdered(9);
        aList.addOrdered(7);
        aList.addOrdered(5);
        aList.addOrdered(3);
        aList.addOrdered(1);
        System.out.println(aList);
        LinkedList.Entry<Integer> remove = aList.remove(3);
//        System.out.println(remove.t);
        System.out.println(aList);

    }




    @Test
    void mergeOrderedLinkedList() {
        LinkedList<Integer> aList = new LinkedList<>();
        aList.addOrdered(9);
        aList.addOrdered(7);
        aList.addOrdered(5);
        aList.addOrdered(3);
        aList.addOrdered(1);
        System.out.println(aList);

        LinkedList<Integer> bList = new LinkedList<>();
        bList.addOrdered(8);
        bList.addOrdered(6);
        bList.addOrdered(5);
        bList.addOrdered(4);
        bList.addOrdered(11);
        bList.addOrdered(-1);
        bList.addOrdered(0);
        System.out.println(bList);

        LinkedList<Number> mergedResult = LinkedList.mergeOrderedLinkedList(aList, bList);
        System.out.println(mergedResult);


    }

    @Test
    public void testAdd() {
        LinkedList<Integer> intList = new LinkedList<>();
        intList.add(5);
        intList.add(3);
        intList.add(2);
        intList.add(1);
        intList.add(4);
        System.out.println(intList);
        intList.revert();
        System.out.println(intList);
    }

    @Test
    public void testAddOrdered() {
        LinkedList<Integer> intList = new LinkedList<>();
        intList.addOrdered(5);
        intList.addOrdered(3);
        intList.addOrdered(2);
        intList.addOrdered(1);
        intList.addOrdered(4);
        System.out.println(intList);
        intList.revert();
        System.out.println(intList);
    }
}