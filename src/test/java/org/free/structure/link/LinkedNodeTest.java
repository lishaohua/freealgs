package org.free.structure.link;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LinkedNodeTest {


    @Test
    void isInLoop() {
        LinkedNode loopLink = buildALoopLink();
        assertTrue(loopLink.isInLoop());

    }

    @Test
    void noLoop() {
        LinkedNode loopLink = buildNoLoopLink();
        assertFalse(loopLink.isInLoop());

    }

    public LinkedNode buildALoopLink() {
        LinkedNode<String> a = new LinkedNode<>("A");
        LinkedNode<String> b = new LinkedNode<>("B");
        LinkedNode<String> c = new LinkedNode<>("C");
        LinkedNode<String> d = new LinkedNode<>("D");
        LinkedNode<String> e = new LinkedNode<>("E");
        LinkedNode<String> f = new LinkedNode<>("F");
        a.setNext(b);
        b.setNext(c);
        c.setNext(d);
        d.setNext(e);
        e.setNext(f);
        f.setNext(c);
        return a;
    }

    public LinkedNode buildNoLoopLink() {
        LinkedNode<String> a = new LinkedNode<>("A");
        LinkedNode<String> b = new LinkedNode<>("B");
        LinkedNode<String> c = new LinkedNode<>("C");
        LinkedNode<String> d = new LinkedNode<>("D");
        LinkedNode<String> e = new LinkedNode<>("E");
        LinkedNode<String> f = new LinkedNode<>("F");
        a.setNext(b);
        b.setNext(c);
        c.setNext(d);
        d.setNext(e);
        e.setNext(f);
        return a;
    }

}