package org.free.others;

public class Feibernach {

    static int f(int n) {
        if (n == 1) return 1;
        if (n == 2) return 1;
        return f(n - 1) + f(n - 2);
    }

    static int ff(int n) {
        if (n == 1) return 1;
        if (n == 2) return 1;
        int a, b = 1, c = 1;
        for (int i = 3; i <= n; i++) {
            a = b;
            b = c;
            c = a + b;
        }
        return c;
    }

    static int sum(int n) {
        int sum = 0;
        for (int i = 1; i <= n; i++) {
            sum = sum + i;
        }
        return sum;
    }

    public static void main(String[] args) {
        int f = 46;
        long l = System.currentTimeMillis();
        System.out.println(f(f));
        long l1 = System.currentTimeMillis();
        System.out.println(ff(f));
        long l2 = System.currentTimeMillis();
        System.out.println("time 1 :" + (l1 - l));
        System.out.println("time 2 :" + (l2 - l1));
        System.out.println(sum(f));
    }
}
