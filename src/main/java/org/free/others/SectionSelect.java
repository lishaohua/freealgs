package org.free.others;


import java.util.ArrayList;
import java.util.List;

/**
 * 区间选择
 * 贪心算法练习
 */
public class SectionSelect {
    static class Section {
        int l;
        int r;

        public Section(int l, int r) {
            this.l = l;
            this.r = r;
        }

        @Override
        public String toString() {
            return l + " <---> " + r;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Section section = (Section) o;

            if (l != section.l) return false;
            return r == section.r;
        }

        @Override
        public int hashCode() {
            int result = l;
            result = 31 * result + r;
            return result;
        }
    }

    public static void main(String[] args) {
        List<Section> list = new ArrayList<Section>(6) {
            {
                add(new Section(1, 5));
                add(new Section(2, 4));
                add(new Section(3, 5));
                add(new Section(5, 9));
                add(new Section(6, 8));
                add(new Section(8, 10));
            }
        };
        List<Section> result = new ArrayList<>();
        int rmax = 0;
        while (rmax < 10 && list.size() > 0) {
            removeNo(list, rmax);
            int selectIndex = minR(list);
            Section selectSection = list.remove(selectIndex);
            rmax = selectSection.r;
            result.add(selectSection);
        }

        System.out.println(result);
    }

    private static void removeNo(List<Section> list, int lmax) {
        List<Section> toRemoveList = new ArrayList<>();
        for (Section s : list) {
            if (s.l < lmax) {
                toRemoveList.add(s);
            }
        }
        list.removeAll(toRemoveList);
    }

    private static int minR(List<Section> list) {
        int min = list.get(0).r;
        int minIndex = 0;
        for (int i = 1; i < list.size(); i++) {
            if (list.get(i).r < min) {
                min = list.get(i).r;
                minIndex = i;
            }
        }
        return minIndex;
    }
}

