package org.free.find;

public class Find {
    /**
     * 二分查找
     *
     * @param a
     * @param v
     * @return
     */
    public static int bsearch(Comparable[] a, Comparable v) {
        int low = 0;
        int high = a.length - 1;

        while (low <= high) { // 注意点1，必须是小于等于，否则可能漏掉mid == low == high的情况
            int mid = low + ((high - low) >> 1);
            int compareResult = a[mid].compareTo(v); // 注意点2
            if (compareResult == 0) {
                return mid;
            } else if (compareResult < 0) {
                low = mid + 1; // 注意不是 mid
            } else if (compareResult > 0) {
                high = mid - 1; // 注意不是 mid
            }
        }
        return -1;
    }

    public static int bsearchFirst(Comparable[] a, Comparable v) {
        int low = 0;
        int high = a.length - 1;
        while (low <= high) {
            int mid = low + ((high - low) >> 1);
            int compareResult = a[mid].compareTo(v);
            if (compareResult < 0) {
                low = mid + 1;
            } else if (compareResult > 0) {
                high = mid - 1;
            } else {
                // 关键点
                if (mid == 0 || a[mid - 1].compareTo(v) != 0) {
                    return mid;
                } else {
                    high = mid - 1;
                }
            }
        }
        return -1;
    }

    public static int bsearchLast(Comparable[] a, Comparable v) {
        int low = 0;
        int high = a.length - 1;
        while (low <= high) {
            int mid = low + ((high - low) >> 1);
            int cResult = a[mid].compareTo(v);
            if (cResult < 0) {
                low = mid + 1;
            } else if (cResult > 0) {
                high = mid - 1;
            } else {
                if (mid == a.length - 1 || a[mid + 1].compareTo(v) != 0) {
                    return mid;
                } else {
                    low = mid + 1;
                }
            }
        }
        return -1;
    }

    public static int bsearchFistGt(Comparable[] a, Comparable v) {
        int low = 0;
        int high = a.length - 1;
        while (low <= high) {
            int mid = low + ((high - low) >> 1);
            int cResult = a[mid].compareTo(v);
            if (cResult < 0) {
                low = mid + 1;
            } else {
                if (mid == 0 || a[mid - 1].compareTo(v) < 0) {
                    return mid;
                } else {
                    high = mid - 1;
                }
            }
        }
        return -1;
    }


    public static void main(String[] args) {
        int[] a = new int[3];
        for (int i = 0; i < a.length; i++) {
            System.out.println(a[i]);
        }
    }


}
