package org.free.sort;


public class SortFactory {

    public static Sortable getInstance(SortType sortType, Comparable[] a) {
        Sortable result = null;
        switch (sortType) {
            case Selection:
                result = new Selection(a);
                break;
            case Insertion:
                result = new Insertion(a);
                break;
            case Insertion2:
                result = new Insertion2(a);
                break;
            case Shell:
                result = new Shell(a);
                break;
            case Merge:
                result = new Merge(a);
                break;
            case MergeBu:
                result = new MergeBu(a);
                break;
            case Quick:
                result = new Quick(a);
                break;
            case Stack:
                result = new HeapSort(a);
                break;
        }
        return result;
    }
}

enum SortType {
    Selection,
    Insertion,
    Insertion2,
    Shell,
    Merge,
    MergeBu,
    Quick,
    Stack
}
