package org.free.sort;

public class Bubble extends AbstractSort {
    public Bubble(Comparable[] a) {
        super(a);
    }

    @Override
    public void sort() {
        for (int i = a.length - 1; i >= 0; i--) {
            for (int j = 0; j < i; j++) {
                if (!less(j, j + 1))
                    swap(j, j + 1);
            }
        }
    }
}
