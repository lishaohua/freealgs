package org.free.sort;

public interface Sortable {
    /**
     * 按升序排列
     */
    void sort();
    void show();
    boolean isSorted();
}
