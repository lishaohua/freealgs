package org.free.sort;

import org.free.util.StdRandom;

public class Quick3way extends AbstractSort {
    public Quick3way(Comparable[] a) {
        super(a);
    }

    @Override
    public void sort() {
        StdRandom.shuffle(a);
        sort(0, a.length - 1);

    }

    private void sort(int lo, int hi) {
        if (hi <= lo) return;
        int lt = lo, i = lo + 1, gt = hi;
        Comparable v = a[lo];
        while (i <= gt) {
            int comp = a[i].compareTo(v);
            if (comp < 0) swap(lt++, i++);
            else if (comp > 0) swap(i, gt--);
            else i++;
        }
        sort(lo, lt - 1);
        sort(gt + 1, hi);
    }
}
