package org.free.sort;

/**
 * 算法名称：插入排序
 * 时间复杂度：最好N，最坏N²
 * 基本思想：
 * <p>
 * 算法特点：
 * 1.排入排序所需的时间取决于输入中的元素的初始顺序
 * 2.不适合随机顺序的数组或逆序数据进行排序
 */

public class Insertion2 extends AbstractSort {
    public Insertion2(Comparable[] a) {
        super(a);
    }

    @Override
    public void sort() {
        for (int i = 1; i < a.length; i++) {
            Comparable min = a[i];
            for (int j = i; j > 0; j--) {
                if (less(min, a[j - 1])) {
                    a[j] = a[j - 1];
                } else {
                    a[j] = min;
                    break;
                }
                if (j - 1 == 0) {
                    a[j - 1] = min;
                }
            }

        }
    }

    public void sort2() {

    }
}
