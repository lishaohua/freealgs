package org.free.sort;


/**
 * 所有排序算法的抽象
 */
public abstract class AbstractSort implements Sortable {
    protected Comparable[] a;

    public AbstractSort(Comparable[] a) {
        this.a = a;
    }


    public boolean less(Comparable v, Comparable w) {
        return v.compareTo(w) < 0;
    }

    public boolean less(int i, int j) {
        return a[i].compareTo(a[j]) < 0;
    }

    public void swap(int i, int j) {
        Comparable t = this.a[i];
        this.a[i] = this.a[j];
        a[j] = t;
    }

    public void show() {
        for (int i = 0; i < a.length; i++) {
            System.out.print(a[i] + " ");
        }
        System.out.println();
    }

    public boolean isSorted() {
        for (int i = 1; i < a.length; i++) {
            if (less(a[i], a[i - 1]))
                return false;
        }
        return true;
    }
}
