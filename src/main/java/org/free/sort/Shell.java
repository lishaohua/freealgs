package org.free.sort;

/**
 * 算法名称：希尔排序
 * 时间复杂度：
 * 基本思想：
 * <p>
 * 插入排序只会交换相邻的元素，因此元素只会一点一点的从数组一端移动到另一端，
 * 希尔排序为了改进插入排序，使数组中任意间隔为h的元素都是有序的，这样的数组称为h有序数组。
 * <p>
 * 算法特点：
 * <p>
 * 1.希尔排序权衡了子数组的规模和有序性。
 * 2.适合大型的数组，对任意排序的数组表现良好
 */

public class Shell extends AbstractSort {
    public Shell(Comparable[] a) {
        super(a);
    }

    public void sort() {
        int n = a.length;
        int h = 1;
        while (h < n / 3) h = h * 3 + 1;
        while (h >= 1) {
            for (int i = h; i < n; i++) {
                for (int j = i; j >= h && less(a[j], a[j - h]); j -= h) {
                    swap(j, j - h);
                }
            }
            h = h / 3;
        }
    }
}
