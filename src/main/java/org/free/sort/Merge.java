package org.free.sort;

/**
 * 简单的递归实现的归并排序
 * <p>
 * 归并排序：分治思想，自下而上，先处理子问题，再合并
 */
public class Merge extends AbstractMergeSort {

    public Merge(Comparable[] a) {
        super(a);
    }

    @Override
    public void sort() {
        sort(0, a.length - 1);
    }

    private void sort(int lo, int hi) {
        if (hi <= lo) {
            return;
        }
        int mid = lo + (hi - lo) / 2;
        sort(lo, mid);
        sort(mid + 1, hi);
        merge(lo, mid, hi);
    }
}
