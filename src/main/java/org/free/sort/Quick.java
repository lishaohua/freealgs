package org.free.sort;

import org.free.util.StdRandom;

/**
 * 快速排序算法：分治思想，自上而下
 * 先分区，再处理子问题
 */
public class Quick extends AbstractSort {
    public Quick(Comparable[] a) {
        super(a);
    }

    @Override
    public void sort() {
        StdRandom.shuffle(a);
        sort(0, a.length - 1);
    }

    private void sort(int lo, int hi) {
        if (hi <= lo) {
            return;
        }
        int j = partition(lo, hi);
        sort(lo, j - 1);
        sort(j + 1, hi);
    }

    /**
     * 分区
     *
     * @param lo 低位指针
     * @param hi 高位指针
     * @return 分区的中间（大小）位置
     */
    private int partition(int lo, int hi) {
        int i = lo, j = hi + 1; // 1. 先声明低位指针和高位指针
        Comparable v = a[lo]; // 2. 申明保存中间（大小）元素的变量
        while (true) {
            while (less(a[++i], v)) if (i == hi) break;
            while (less(v, a[--j])) if (j == lo) break;
            if (i >= j) break;
            swap(i, j);
        }
        swap(lo, j);
        return j;
    }
}
