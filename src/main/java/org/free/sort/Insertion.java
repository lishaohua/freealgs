package org.free.sort;

/**
 * 算法名称：插入排序
 * 时间复杂度：最好N，最坏N²
 * 基本思想：
 * <p>
 * 算法特点：
 * 1.排入排序所需的时间取决于输入中的元素的初始顺序
 * 2.不适合随机顺序的数组或逆序数据进行排序
 */

public class Insertion extends AbstractSort {
    public Insertion(Comparable[] a) {
        super(a);
    }

    public void sort() {
        for (int i = 1; i < a.length; i++) {
            for (int j = i; j >0 && less(a[j], a[j-1]); j--) {
                swap(j, j-1);
            }
        }
    }
}
