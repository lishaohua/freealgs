package org.free.sort;

/**
 * 当一棵二叉树的每个节点都大于等于它的两个子节点时，它被称为堆有序
 */
public class HeapSort extends AbstractSort {
    public HeapSort(Comparable[] a) {
        super(a);
    }

    @Override
    public void sort() {
        int n = a.length - 1;
        for (int k = n / 2; k >= 1; k--) {
            sink(k, n);
        }


        while (n > 1) {
            swap(1, n--);
            sink(1, n);
        }

    }

    private boolean isMaxHeap(int k) {
        int n = a.length;
        if (k > n) return true;
        int left = 2 * k;
        int right = 2 * k + 1;
        if (left <= n && less(k, left)) return false;
        if (right <= n && less(k, right)) return false;
        return isMaxHeap(left) && isMaxHeap(right);
    }

    private void swim(int k) {
        while (k > 1 && less(k / 2, k)) {
            swap(k / 2, k);
            k = k / 2;
        }
    }

    private void sink(int k, int n) {
        while (2 * k <= n) {
            int j = 2 * k;
            if (j < n && less(j, j + 1)) j++;
            if (!less(k, j)) break;
            swap(k, j);
            k = j;
        }
    }
}
