package org.free.sort;

/**
 * 循环改造后的递归实现
 */
public class MergeBu extends AbstractMergeSort {
    public MergeBu(Comparable[] a) {
        super(a);
    }

    @Override
    public void sort() {
        int n = a.length;
        for (int sz = 1; sz < n; sz = sz + sz) {
            for (int lo = 0; lo < n - sz; lo += sz + sz) {
                merge(lo, lo + sz - 1, Math.min(lo + sz + sz - 1, n - 1));
            }
        }
    }
}
