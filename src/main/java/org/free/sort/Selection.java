package org.free.sort;

/**
 * 算法名称：选择排序
 * 时间复杂度：N²
 * 基本思想：
 * 首先，找到数组中最小的元素，其次和数组的第一个元素交换位置，再次在剩下的元素中查找最小元素，将它和数组的第二个元素交换位置。
 * 如此反复直至整个数组排序。
 * 算法特点：
 * 1.运行时间和输入无关；
 * 2.数据移动次数最少
 */
public class Selection extends AbstractSort {

    public Selection(Comparable[] a) {
        super(a);
    }

    public void sort() {
        int n = a.length;
        for (int i = 0; i < n; i++) {
            int min = i;
            for (int j = i + 1; j < n; j++) {
                if (less(a[j], a[min])) min = j;
            }
            swap(i, min);
        }
    }
}
