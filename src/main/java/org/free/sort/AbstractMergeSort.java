package org.free.sort;

/**
 * 归并排序：分治思想，自下而上，先处理子问题，再合并
 */
public abstract class AbstractMergeSort extends AbstractSort {
    protected Comparable[] aux;

    public AbstractMergeSort(Comparable[] a) {
        super(a);
        aux = new Comparable[a.length];
    }

    protected void merge(int lo, int mid, int hi) {
        // 数组复制一份，所以不是原地排序
        for (int k = lo; k <= hi; k++) {
            aux[k] = a[k];
        }

        int i = lo, j = mid + 1; // i为低位的指针，j为高位的指针
        for (int k = lo; k <= hi; k++) { // 先处理1 2两种边界情况
            if (i > mid) a[k] = aux[j++]; // 1.低位高于中间位时，直接复制高位数组
            else if (j > hi) a[k] = aux[i++]; // 2.高位超越时，直至复制低位数组
            else if (less(aux[j], aux[i])) a[k] = aux[j++]; // 3.如果高位小于低位，则填充高位值，并将高位指针向后移动
            else a[k] = aux[i++]; // 4.如果高位大于等于低位，则填充低位的值，并将低位指针向后移动
        }
    }
}
