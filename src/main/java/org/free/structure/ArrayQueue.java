package org.free.structure;

public class ArrayQueue<E> {
    private Object[] elementData;
    private int size;
    private int header;
    private int tail;

    public ArrayQueue(int size) {
        this.size = size;
        this.elementData = new Object[size];
        header = 0;
        tail = 0;
    }

    // 入队操作，将 item 放入队尾
    public boolean enqueue(String item) {
        // tail == n 表示队列末尾没有空间了
        if (tail == size) {
            // tail ==n && head==0，表示整个队列都占满了
            if (header == 0) return false;
            // 数据搬移
            for (int i = header; i < tail; ++i) {
                elementData[i-header] = elementData[i];
            }
            // 搬移完之后重新更新 head 和 tail
            tail -= header;
            header = 0;
        }

        elementData[tail] = item;
        ++tail;
        return true;
    }


    public E dequeue() {
        if (header == tail) {
            return null;
        }
        return (E) elementData[header++];
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("[");
        for (Object e : this.elementData) {
            sb.append(e).append(",");
        }
        sb.append("]");
        return sb.toString();
    }
}
