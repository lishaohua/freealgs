package org.free.structure.link;

/**
 * 链表结构
 *
 * @param <T> 数据类型
 */
public class LinkedNode<T> {
    private T data;
    private LinkedNode<T> next;

    public LinkedNode(T data) {
        this.data = data;
    }

    public LinkedNode(T data, LinkedNode<T> next) {
        this.data = data;
        this.next = next;
    }

    public LinkedNode<T> setNext(LinkedNode<T> next) {
        this.next = next;
        return next;
    }

    @Override
    public String toString() {
        if (isInLoop()) {
            return "this link is detected loop,can't output";
        }
        StringBuilder sb = new StringBuilder();
        LinkedNode<T> p = this;
        do {
            sb.append(p.data).append("->");
            p = p.next;
        } while (p != null);
        sb.append("NULL");
        return sb.toString();
    }

    /**
     * 通过快慢指针判断是否在循环之中
     *
     * @return
     */
    public boolean isInLoop() {
        if (this.next == null) {
            return false;
        }
        LinkedNode fast = this;
        LinkedNode slow = this;
        while (true) {
            fast = fast.next.next;
            slow = slow.next;
            if (fast == null) {
                return false;
            }
            if (fast == slow) {
                return true;
            }
        }
    }
}