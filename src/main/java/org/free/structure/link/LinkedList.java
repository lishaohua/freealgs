package org.free.structure.link;

/**
 * 链表
 *
 * @param <T>
 */
public class LinkedList<T extends Number> {
    private Entry<T> head; // 头指针

    static class Entry<T> {
        T t; // 元素
        Entry<T> next;

        public Entry(T t) {
            this.t = t;
        }

        @Override
        public String toString() {
            return "[" + this.t + "]";
        }
    }

    public void revert() {
        Entry<T> current = head.next; // 当前指针指向下一个节点
        head.next = null; //头节点的next指针切断，成为最后一个节点
        while (current != null) {
            Entry<T> currentNext = current.next; //
            current.next = head;
            head = current;
            current = currentNext;
        }
    }

    /**
     * 添加一个元素到链表头
     *
     * @param t
     */
    public void add(T t) {
        Entry<T> entry = new Entry<>(t);
        if (head == null) {
            head = entry;
        } else {
            entry.next = head;
            head = entry;
        }
    }

    public Entry<T> getHead() {
        return head;
    }

    @Override
    public String toString() {
        Entry<T> current = head;
        StringBuilder sb = new StringBuilder();
        while (current != null) {
            sb.append("[").append(current.t).append("]").append("->");
            current = current.next;
        }
        sb.append("null");
        return sb.toString();
    }

    /**
     * 删除一个元素
     *
     * @param index
     * @return
     */
    public Entry<T> remove(int index) {
        if (index < 0) {
            throw new IllegalArgumentException("i must be zero or a positive integer");
        }
        int i = 0;
        Entry<T> current = this.head;
        Entry<T> previous = this.head;
        while (true) {
            if (current == null) {
                return null;
            }
            if (i++ == index) {
                previous.next = current.next;
                return current;
            }
            previous = current;
            current = current.next;
        }
    }


    /**
     * 获得中位值
     *
     * @return 中位节点指针
     */
    public Entry<T> getMedian() {
        Entry slow = head;
        Entry fast = head;
        while (fast != null && fast.next != null) {
            slow = slow.next;
            fast = fast.next.next;
        }
        return slow;
    }

    /**
     * 两个有序链表
     *
     * @param a
     * @param b
     * @return
     */
    public static LinkedList<Number> mergeOrderedLinkedList(LinkedList a,
                                                            LinkedList b) {
        if (a == null && b == null) {
            return null;
        }
        if (a == null && b != null) {
            return b;
        }
        if (b == null && a != null) {
            return a;
        }
        LinkedList<Number> mergeResult = new LinkedList<>();
        Entry<Number> aCurrent = a.head;
        Entry<Number> bCurrent = b.head;
        while (aCurrent != null || bCurrent != null) {
            if (aCurrent.t.doubleValue() < bCurrent.t.doubleValue()) {
                mergeResult.addOrdered(aCurrent.t);
                aCurrent = aCurrent.next;
            } else {
                mergeResult.addOrdered(bCurrent.t);
                bCurrent = bCurrent.next;
            }
            if (aCurrent == null && bCurrent != null) {
                while (bCurrent != null) {
                    mergeResult.addOrdered(bCurrent.t);
                    bCurrent = bCurrent.next;
                }
            }
            if (bCurrent == null && aCurrent != null) {
                while (aCurrent != null) {
                    mergeResult.addOrdered(aCurrent.t);
                    aCurrent = aCurrent.next;
                }
            }
        }
        return mergeResult;

    }

    /**
     * 添加一个元素到有序链表
     *
     * @param t
     */
    public void addOrdered(T t) {
        Entry<T> entry = new Entry<>(t);
        if (head == null) {
            head = entry;
            return;
        }
        Entry<T> current = head;
        Entry<T> previous = current;

        while (current != null && t.doubleValue() > current.t.doubleValue()) {
            previous = current;
            current = current.next;
        }

        // 待添加的值最小，将值插入链表头
        if (current == previous) {
            entry.next = head;
            head = entry;
        } else { // 插入逻辑
            previous.next = entry;
            entry.next = current;
        }
    }


}
