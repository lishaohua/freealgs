package org.free.structure;

/**
 * @param <E>
 */
public class ArrayStack<E> {
    private int count;
    private Object[] elementData;
    private int size;

    public ArrayStack(int size) {
        this.size = size;
        this.count = 0;
        this.elementData = new Object[size];
    }

    public boolean push(E e) {
        if (count == size) {
            return false;
        }
        this.elementData[count] = e;
        this.count++;
        return true;
    }

    public E pop() {
        if (count == 0) {
            return null;
        }
        E result = (E) this.elementData[count];
        this.count--;
        return result;
    }
}
