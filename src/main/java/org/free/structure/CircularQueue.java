package org.free.structure;

public class CircularQueue<E> {
    private Object[] elementData;
    private int size;
    private int head = 0;
    private int tail = 0;

    public CircularQueue(int size) {
        this.size = size;
        this.elementData = new Object[size];
    }

    public boolean enqueue(E e) {
        // 队列已满的判断条件
        if ((tail + 1) % size == head) {
            return false;
        }
        elementData[tail] = e;
        tail = (tail + 1) % size;
        return true;
    }

    public E dequeue() {
        if (head == tail) return null;
        E result = (E) elementData[head];
        head = (head + 1) % size;
        return result;
    }


}
